variable "ec2_instance_type" {
  type        = string
  default     = "t3.micro"
  description = "This is the AWS instance type/ size to use."
}

variable "ec2_ami_id" {
  type        = string
  description = "The AMI ID to use to launch the instance."
}

variable "ec2_instance_name" {
  type        = string
  description = "Name of the instance"
}