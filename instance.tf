resource "aws_instance" "name" {
  #source = "git::https://gitlab.com/infra-gLo/AWS/modules/ec2-instance.git"
  ami              = var.ec2_ami_id
  instance_type     = var.ec2_instance_type

  tags = {
    Name = var.ec2_instance_name
  }
  
}