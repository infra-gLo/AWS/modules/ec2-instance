# EC2-Instance

This is a simple AWS EC2 instance module for Terraform. Copy to your main tf file.

```
module "instance" {
  source            = "git::https://gitlab.com/infra-gLo/AWS/modules/ec2-instance.git"
  ami               = < your AMI ID >
  instance_type     = "t3.micro"
  ec2_instance_name = "My Instance"

}

```
